### POC de Ejemplo del acceso a Elastic Search con java api

## Prerrequisitos

1. Clonar el proyecto de Infraestrura ElasticSearch + Kibana

git clone https://gitlab.com/tanoka12/docker-geo.git

- Seguir sus instrucciones para levantar los contenedores con ES + Kibana


## Documentacion

1. Ejemplos-Rest-ElasticSearch.md

Ejemplos para lanzar desde la consola de Kibana

2. Ejemplos-Java-ElasticSearch.md

Ejemplos java para lanzar desde el IDE.

