
## Ejemplos de Operaciones sobre ElasticSearch con el API-Java


1. Create Client (Low Level)

````java
    @Bean
    public RestClient restClient() {
        RestClientBuilder builder = RestClient.builder(new HttpHost("192.168.99.100", 9200));
        return builder.build();
    }
````

2. Create High Level Clients

`````java
public class ElasticSearchClientImpl extends RestHighLevelClient implements ElasticSearchClient {

    protected RestClient lowLevelClient;

    private final ObjectMapper mapper;

    public ElasticSearchClientImpl(RestClient restClient, ObjectMapper mapper) {
        super(restClient);
        this.lowLevelClient = restClient;
        this.mapper = mapper;
    }
`````


### Operaciones con Indices - IndexOperationsIT

1. Crear Indice

`````java

    @Test
    public void shouldCreateIndex() throws Exception {

        String index = generateIndexWithDateSuffix(PREFFIX_INDEX);

        System.out.println("::: Create Index: " + index);

        final boolean created = client.createIndex(index, indexSource);

        assertThat(created, equalTo(true));

    }
`````

- Implementación de la operación:

```java
    @Override
    public boolean createIndex(String index, String indexSettings) throws Exception {

        StringEntity entity = null;
        if (!Strings.isNullOrEmpty(indexSettings)) {
            entity = new StringEntity(indexSettings, ContentType.APPLICATION_JSON);
        }

        Response response = getLowLevelClient().performRequest(PUT_METHOD, "/" + index, Collections.emptyMap(), entity);

        return getStatusCode(response) == HTTP_OK;
    }
```


2. Obtener el ultimo indice creado

````java
   @Test
    public void shouldReturnRecentIndex() throws Exception {

        String index = generateIndexWithDateSuffix(PREFFIX_INDEX);

        client.createIndex(index, indexSource);

        final String recentIndex = client.getRecentIndexByPrefix(PREFFIX_INDEX + "*");

        System.out.println("::: Recent Index is:" + recentIndex);

        assertThat(recentIndex, equalTo(index));
    }
````

- Implementación de la operación

```java

    @Override
    public String getRecentIndexByPrefix(String preffixIndex) throws Exception {

        final Response response = getLowLevelClient().performRequest(GET_METHOD, "/" + preffixIndex + SETTINGS,
                new HashMap<>());

        final String recentIndexByCreationTime = getRecentIndexByCreationTime(response);

        return recentIndexByCreationTime;
    }
```

### Operaciones de Insercion - InsertOperationsIT

1. Inserciones Masivas con BULK

````java
    @Test
    public void shouldInsertLocations() throws Exception{

        final List<GeoRequest> geoRequests = this.buildGeoRequest();

        final String index = dao.getRecentIndexByPrefix(PREFFIX_INDEX + "*");

        final List<ErrorESResponse> errorESResponses = dao.insertLocations(index, "geolocation", geoRequests);

        assertThat(errorESResponses, hasSize(0));
    }
````

- Implementación de la operación

```java
    @Override
    public List<ErrorESResponse> insertLocations(String index, String mapping, List<? extends ESRequest> locations) throws Exception {
        List<ErrorESResponse> errors = new ArrayList<>();

        if (!locations.isEmpty()) {
            BulkRequest bulkRequest = new BulkRequest();

            locations.forEach(location -> {
                try {
                    bulkRequest.add(new IndexRequest(index, mapping, location.getIndexId())
                            .source(mapper.writeValueAsString(location), XContentType.JSON));
                } catch (JsonProcessingException e) {
                    throw new RuntimeException(e);
                }
            });
            BulkResponse bulkResponse = client.bulk(bulkRequest);
            if (bulkResponse.hasFailures()) {
                errors = buildErrorsResponse(bulkResponse.getItems());
            }

        }
        return errors;

    }
```

### Operaciones de Búsqueda - SearchOperationsIT

1. Busqueda por: ViaType + ViaName + Number + CodPostal

````java
    @Test
    public void shouldSearchLocationByViaName() throws Exception {


        GeoRequest location = new GeoRequestBuilder()
                .viaType("Avenida").viaName("Rey Juan Carlos I")
                .number("1").postalcode("52001").build();
        
        final List<GeoResponse> geoResponses = dao.searchLocation(location, INDEX_CARTO, MAPPING);


        assertThat(geoResponses, hasSize(1));
    }
````

- Implementación

```java
    public List<GeoResponse> searchLocation(GeoRequest location, String index, String mapping) throws Exception {


        List<GeoResponse> locations = new ArrayList<>();

        MatchQueryBuilder builder = QueryBuilders.matchQuery("geo_all", buildQueryString(location))
                .operator(Operator.AND)
                .fuzziness(0);

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder().size(500).query(builder);


        SearchRequest searchRequest = new SearchRequest(index).types(mapping).source(searchSourceBuilder);


        System.out.println("::: Query ES  in index : " + index + " " + searchRequest);


        SearchResponse response = client.search(searchRequest);


        System.out.println("::: Response ES :" + response);


        if (response.getHits().getTotalHits() > 0) {

            locations = extractResponse(response, GeoResponse.class);

        }


        return locations;
    }
```


2. Búsqueda por Geolocalización

`````java
    @Test
    public void shouldSearchNearestLocations() throws Exception{
        GeoRequest location = new GeoRequestBuilder().latitude("35.292602469252195").longitude("-2.93876416598411").build();

        final List<AtmResponse> atmResponses = dao.searchNearestLocation(INDEX_CAJEROS, MAPPING, location, BUFFER);

        assertThat(atmResponses, hasSize(5));
    }
`````

- Implementación

````java
    @Override
    public List<AtmResponse> searchNearestLocation(String uuidIndex, String mapping, GeoRequest routingLocation, String buffer) throws Exception {

        List<AtmResponse> atmResponses = new ArrayList<>();

        GeoDistanceQueryBuilder qb = QueryBuilders
                .geoDistanceQuery("location")
                .point(Double.parseDouble(routingLocation.getLocation().getLat()),
                        Double.parseDouble(routingLocation.getLocation().getLon()))
                .distance(Double.parseDouble(buffer), DistanceUnit.METERS);


        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder().size(500).query(qb);

        System.out.println("::: Query ES  in index : " + uuidIndex + " " + searchSourceBuilder);

        SearchRequest searchRequest = new SearchRequest(uuidIndex).types(mapping).source(searchSourceBuilder);


        SearchResponse response = client.search(searchRequest);

        System.out.println("::: Response ES :" + response);

        if (response.getHits().getTotalHits() > 0) {
            atmResponses = extractResponse(response, AtmResponse.class);
            System.out.println(atmResponses.size());
        }

        return atmResponses;

    }
````


3. Busqueda con Composición Bool

````java
    @Test
    public void shouldReturnByCategoryAndSuccess() throws Exception{

        final List<AtmResponse> atmResponses = dao.searchByCategory(INDEX_CAJEROS, MAPPING, "BBVA", "2");

        assertThat(atmResponses, hasSize(2));


    }
````

- Implementación

````java
    @Override
    public List<AtmResponse> searchByCategory(String index, String mapping, String category, String success) throws Exception{

        List<AtmResponse> atmResponses = new ArrayList<>();

        BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();

        boolQuery.must( QueryBuilders.matchQuery( "subcategory", category ))
                .must(QueryBuilders.matchQuery( "success", success ));

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder().size(500).query(boolQuery);

        System.out.println("::: Query ES  in index : " + index + " " + searchSourceBuilder);

        SearchRequest searchRequest = new SearchRequest(index).types(mapping).source(searchSourceBuilder);


        SearchResponse response = client.search(searchRequest);

        System.out.println("::: Response ES :" + response);

        if (response.getHits().getTotalHits() > 0) {
            atmResponses = extractResponse(response, AtmResponse.class);
            System.out.println(atmResponses.size());
        }

        return atmResponses;
    }
````

