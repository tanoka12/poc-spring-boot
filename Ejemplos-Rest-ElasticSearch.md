
#### Ejemplos del API Rest de ElasticSearch

1. Creación de indices

- PUT /videoclub

````json
{
    "settings" : {
        "number_of_shards" : 1
    },
    "mappings" : {
        "peliculas" : {
            "properties" : {
                "nombre" : { "type" : "text" },
                "director" : { "type" : "text" },
                "genero" : { "type" : "text" }
            }
        }
    }
}
````

2. Lectura de Indices

    - GET videoclub

    - GET videoclub/_settings

    - GET videoclub/_mapping

3. Borrado de Indices

    - DELETE videoclub

4. Inserción de Documentos

PUT videoclub/peliculas/1

````json
{
    "nombre": "El Señor de los Anillos",
    "director": "Peter Jackson",
    "genero": "Fantastica"
}

````

POST videoclub/peliculas

````json
{
    "nombre": "Coco",
    "director": "Disney",
    "genero": "Dibujos"
}

````

5. Lectura de documentos

GET videoclub/peliculas/1

6. Actualización de documentos

PUT videoclub/peliculas/1/_update

````json
{
    "doc":
      {
        "nombre": "El Señor de los Anillos, La Comunidad del Anillo",
        "director": "Peter Jackson",
        "genero": "Fantastica"
      }
}

````

7. Borrado de documentos

- DELETE videoclub/peliculas/1

8. Void Search

- POST videoclub/_search

```json
{
  "took": 0,
  "timed_out": false,
  "_shards": {
    "total": 1,
    "successful": 1,
    "skipped": 0,
    "failed": 0
  },
  "hits": {
    "total": 2,
    "max_score": 1,
    "hits": [
      {
        "_index": "videoclub",
        "_type": "peliculas",
        "_id": "kbNza2EBrWiGHw9rYOJB",
        "_score": 1,
        "_source": {
          "nombre": "Coco",
          "director": "Disney",
          "genero": "Dibujos"
        }
      },
      {
        "_index": "videoclub",
        "_type": "peliculas",
        "_id": "1",
        "_score": 1,
        "_source": {
          "nombre": "El Señor de los Anillos",
          "director": "Peter Jackson",
          "genero": "Fantastica"
        }
      }
    ]
  }
}
```


9. BULK

- POST _bulk
````
{ "create" : { "_index" : "videoclub", "_type" : "peliculas", "_id" : "2" } }
{ "nombre" : "La Comunidad", "director" : "Alex de la Iglesia", "genero" : "Terror" }
{ "create" : { "_index" : "videoclub", "_type" : "peliculas", "_id" : "2" } }
{ "nombre" : "El Hobbit", "director" : "Peter Jackson", "genero" : "Fantastica" }
{ "create" : { "_index" : "videoclub", "_type" : "peliculas", "_id" : "2" } }
{ "nombre" : "Muertos de Risa", "director" : "Alex de la Iglesia", "genero" : "Comedia" }
````

10. Search

POST /videoclub/peliculas/_search

    
 ### Query DSL
   
POST /videoclub/peliculas/_search 
```json

    {
        "query": {
            "match" : {
                "director" : "Alex de la Iglesia"
            }
        }
    }
````

POST /videoclub/peliculas/_search   
```json  
    
    POST videoclub/_search
    {
      "query": {
        "bool" : {
          "must" : {
            "match" : { "director" : "Alex de la Iglesia" }
          },
          "must_not" : {
            "match" : { "genero" : "Comedia" }
          }
        }
      }
    }
````


### Ejemplos de Geolocalización

1. Creamos un indice de Cajeros


PUT geo-cajeros

````json
{
  "settings": {
    "index": {
      "number_of_shards": "2",
      "number_of_replicas": "1"
    }
  },
  "mappings": {
    "geolocation": {
      "properties": {
        "location": {
          "type": "geo_point"
        },
        "id": {
          "type": "text"
        },
        "red4B": {
          "type": "text"
        },
        "red6000": {
          "type": "text"
        },
        "servired": {
          "type": "text"
        },
        "entityCode": {
          "type": "text"
        },
        "subcategory": {
          "type": "keyword"
        },
        "branchCode": {
          "type": "text"
        },
        "branch": {
          "type": "text"
        },
        "atmCode": {
          "type": "text"
        },
        "ordinal": {
          "type": "text"
        },
        "province": {
          "type": "text"
        },
        "state": {
          "type": "text"
        },
        "postalCode": {
          "type": "keyword"
        },
        "town": {
          "type": "text"
        },
        "viaType": {
          "type": "text"
        },
        "viaName": {
          "type": "text"
        },
        "number": {
          "type": "text"
        },
        "rdo": {
          "type": "text"
        },
        "success": {
          "type": "integer"
        },
        "source": {
          "type": "keyword"
        }
      }
    }
  }
}
````

2. Insertamos los Datos.

POST _bulk
````
{"index":{"_index":"geo-cajeros","_type":"geolocation","_id":"21531"}}
{"location":{"lon":"-2.9412414332982597","lat":"35.294448169111895"},"id":"21531","red4B":"0","red6000":"0","servired":"1","entityCode":"2100","branchCode":"1891","branch":"MELILLA","atmCode":"17641","ordinal":"2","province":"MELILLA","state":"MELILLA","postalCode":"52001","town":"MELILLA","viaType":"Avenida","viaName":"Rey Juan Carlos I","number":"28","rdo":null,"success":1,"source":"geo-cartociudad-20170727-125627","subcategory":"CAIXABANK"}
{"index":{"_index":"geo-cajeros","_type":"geolocation","_id":"21534"}}
{"location":{"lon":"-2.94589042897148","lat":"35.273042621779005"},"id":"21534","red4B":"0","red6000":"0","servired":"1","entityCode":"2100","branchCode":"5786","branch":"MELILLA - EL REAL","atmCode":"17642","ordinal":"2","province":"MELILLA","state":"MELILLA","postalCode":"52006","town":"MELILLA","viaType":"Calle","viaName":"Mar Chica","number":"57","rdo":null,"success":1,"source":"geo-cartociudad-20170727-125627","subcategory":"CAIXABANK"}
{"index":{"_index":"geo-cajeros","_type":"geolocation","_id":"37337"}}
{"location":{"lon":"-2.93800555958253","lat":"35.2932693556513"},"id":"37337","red4B":"0","red6000":"1","servired":"0","entityCode":"0182","branchCode":"3430","branch":null,"atmCode":null,"ordinal":"1","province":"MELILLA","state":"MELILLA","postalCode":"52001","town":"MELILLA","viaType":"Calle","viaName":"del Teniente Aguilar de Mera","number":"3","rdo":null,"success":2,"source":"geo-cartociudad-20170727-125627","subcategory":"BBVA"}
{"index":{"_index":"geo-cajeros","_type":"geolocation","_id":"41777"}}
{"location":{"lon":"-2.9395669710429395","lat":"35.2933545674449"},"id":"41777","red4B":"0","red6000":"1","servired":"0","entityCode":"0049","branchCode":"4273","branch":null,"atmCode":null,"ordinal":"1","province":"MELILLA","state":"MELILLA","postalCode":"52001","town":"MELILLA","viaType":"Avenida","viaName":"Rey Juan Carlos I","number":"12","rdo":null,"success":1,"source":"geo-cartociudad-20170727-125627","subcategory":"BANCO SANTANDER"}
{"index":{"_index":"geo-cajeros","_type":"geolocation","_id":"50581"}}
{"location":{"lon":"-2.9402883176288","lat":"35.2940597945448"},"id":"50581","red4B":"0","red6000":"1","servired":"0","entityCode":"0049","branchCode":null,"branch":null,"atmCode":null,"ordinal":"1","province":"MELILLA","state":"MELILLA","postalCode":"52001","town":"MELILLA","viaType":"Plaza","viaName":"Menéndez Pelayo","number":"2","rdo":null,"success":1,"source":"geo-cartociudad-20170727-125627","subcategory":"BANCO SANTANDER"}
{"index":{"_index":"geo-cajeros","_type":"geolocation","_id":"47107"}}
{"location":{"lon":"-2.9466068793071503","lat":"35.286087881584194"},"id":"47107","red4B":"0","red6000":"1","servired":"0","entityCode":"2103","branchCode":null,"branch":null,"atmCode":null,"ordinal":"1","province":"MELILLA","state":"MELILLA","postalCode":"52005","town":"MELILLA","viaType":"Calle","viaName":"Remonta","number":"S/N","rdo":"Hospital Comarcal","success":4,"source":"geo-cartociudad-20170727-125627","subcategory":"UNICAJA BANCO"}
{"index":{"_index":"geo-cajeros","_type":"geolocation","_id":"34662"}}
{"location":{"lon":"-2.93688152228294","lat":"35.2796036230699"},"id":"34662","red4B":"0","red6000":"1","servired":"0","entityCode":"2103","branchCode":"0140","branch":null,"atmCode":null,"ordinal":"1","province":"MELILLA","state":"MELILLA","postalCode":"52006","town":"MELILLA","viaType":"Calle","viaName":"Médico García Martínez","number":"3","rdo":null,"success":3,"source":"geo-cartociudad-20170727-125627","subcategory":"UNICAJA BANCO"}
{"index":{"_index":"geo-cajeros","_type":"geolocation","_id":"38798"}}
{"location":{"lon":"-2.94062246701822","lat":"35.2929364591406"},"id":"38798","red4B":"0","red6000":"1","servired":"0","entityCode":"2103","branchCode":"2031","branch":null,"atmCode":null,"ordinal":"1","province":"MELILLA","state":"MELILLA","postalCode":"52001","town":"MELILLA","viaType":"Calle","viaName":"del General O'Donnell","number":"11","rdo":null,"success":3,"source":"geo-cartociudad-20170727-125627","subcategory":"UNICAJA BANCO"}
{"index":{"_index":"geo-cajeros","_type":"geolocation","_id":"55840"}}
{"location":{"lon":"-2.94119810441898","lat":"35.294070811835006"},"id":"55840","red4B":"0","red6000":"1","servired":"0","entityCode":"0182","branchCode":null,"branch":null,"atmCode":null,"ordinal":"1","province":"MELILLA","state":"MELILLA","postalCode":"52001","town":"MELILLA","viaType":"Calle","viaName":"del General Pareja","number":"11","rdo":null,"success":2,"source":"geo-cartociudad-20170727-125627","subcategory":"BBVA"}
{"index":{"_index":"geo-cajeros","_type":"geolocation","_id":"21532"}}
{"location":{"lon":"-2.9412414332982597","lat":"35.294448169111895"},"id":"21532","red4B":"0","red6000":"0","servired":"1","entityCode":"2100","branchCode":"1891","branch":"MELILLA","atmCode":"17657","ordinal":"3","province":"MELILLA","state":"MELILLA","postalCode":"52001","town":"MELILLA","viaType":"Avenida","viaName":"Rey Juan Carlos I","number":"28","rdo":null,"success":1,"source":"geo-cartociudad-20170727-125627","subcategory":"CAIXABANK"}
{"index":{"_index":"geo-cajeros","_type":"geolocation","_id":"21530"}}
{"location":{"lon":"-2.9412414332982597","lat":"35.294448169111895"},"id":"21530","red4B":"0","red6000":"0","servired":"1","entityCode":"2100","branchCode":"1891","branch":"MELILLA","atmCode":"10664","ordinal":"1","province":"MELILLA","state":"MELILLA","postalCode":"52001","town":"MELILLA","viaType":"Avenida","viaName":"Rey Juan Carlos I","number":"28","rdo":null,"success":1,"source":"geo-cartociudad-20170727-125627","subcategory":"CAIXABANK"}
{"index":{"_index":"geo-cajeros","_type":"geolocation","_id":"21533"}}
{"location":{"lon":"-2.94589042897148","lat":"35.273042621779005"},"id":"21533","red4B":"0","red6000":"0","servired":"1","entityCode":"2100","branchCode":"5786","branch":"MELILLA - EL REAL","atmCode":"17637","ordinal":"1","province":"MELILLA","state":"MELILLA","postalCode":"52006","town":"MELILLA","viaType":"Calle","viaName":"Mar Chica","number":"57","rdo":null,"success":1,"source":"geo-cartociudad-20170727-125627","subcategory":"CAIXABANK"}
{"index":{"_index":"geo-cajeros","_type":"geolocation","_id":"31355"}}
{"location":{"lon":"-2.9473149","lat":"35.2856245"},"id":"31355","red4B":"0","red6000":"1","servired":"0","entityCode":"0182","branchCode":"5601","branch":null,"atmCode":null,"ordinal":"1","province":"MELILLA","state":"MELILLA","postalCode":"52005","town":"MELILLA","viaType":"Calle","viaName":"Remonta","number":"1","rdo":null,"success":1,"source":"geo-openaddress-20170530-170041","subcategory":"BBVA"}
{"index":{"_index":"geo-cajeros","_type":"geolocation","_id":"42886"}}
{"location":{"lon":"-2.93875707586459","lat":"35.27402239786449"},"id":"42886","red4B":"0","red6000":"1","servired":"0","entityCode":"0182","branchCode":"6649","branch":null,"atmCode":null,"ordinal":"1","province":"MELILLA","state":"MELILLA","postalCode":"52006","town":"MELILLA","viaType":"Calle","viaName":"del General Villalba","number":"S/N","rdo":null,"success":4,"source":"geo-cartociudad-20170727-125627","subcategory":"BBVA"}
{"index":{"_index":"geo-cajeros","_type":"geolocation","_id":"36939"}}
{"location":{"lon":"-2.94119810441898","lat":"35.294070811835006"},"id":"36939","red4B":"0","red6000":"1","servired":"0","entityCode":"0081","branchCode":"1290","branch":null,"atmCode":null,"ordinal":"1","province":"MELILLA","state":"MELILLA","postalCode":"52001","town":"MELILLA","viaType":"Calle","viaName":"del General Pareja","number":"11","rdo":null,"success":2,"source":"geo-cartociudad-20170727-125627","subcategory":"BANCO SABADELL"}
{"index":{"_index":"geo-cajeros","_type":"geolocation","_id":"40493"}}
{"location":{"lon":"-2.93972022058747","lat":"35.2934564996134"},"id":"40493","red4B":"0","red6000":"1","servired":"0","entityCode":"0075","branchCode":"0015","branch":null,"atmCode":null,"ordinal":"1","province":"MELILLA","state":"MELILLA","postalCode":"52001","town":"MELILLA","viaType":"Avenida","viaName":"Rey Juan Carlos I","number":"14","rdo":null,"success":1,"source":"geo-cartociudad-20170727-125627","subcategory":"BANCO POPULAR"}
{"index":{"_index":"geo-cajeros","_type":"geolocation","_id":"53800"}}
{"location":{"lon":"-2.93955849060846","lat":"35.285368892376"},"id":"53800","red4B":"0","red6000":"1","servired":"0","entityCode":"0487","branchCode":"3442","branch":null,"atmCode":null,"ordinal":"1","province":"MELILLA","state":"MELILLA","postalCode":"52006","town":"MELILLA","viaType":"Calle","viaName":"Maritimo","number":"5","rdo":null,"success":5,"source":"geo-cartociudad-20170727-125627","subcategory":"BMN"}
{"index":{"_index":"geo-cajeros","_type":"geolocation","_id":"30242"}}
{"location":{"lon":"-2.93887885980745","lat":"35.293308275624895"},"id":"30242","red4B":"0","red6000":"1","servired":"0","entityCode":"2103","branchCode":"0155","branch":null,"atmCode":null,"ordinal":"1","province":"MELILLA","state":"MELILLA","postalCode":"52001","town":"MELILLA","viaType":"Calle","viaName":"Ejército Español","number":"9","rdo":null,"success":3,"source":"geo-cartociudad-20170727-125627","subcategory":"UNICAJA BANCO"}
{"index":{"_index":"geo-cajeros","_type":"geolocation","_id":"52947"}}
{"location":{"lon":"-2.9392362743938896","lat":"35.2760222398866"},"id":"52947","red4B":"0","red6000":"1","servired":"0","entityCode":"2103","branchCode":"0284","branch":null,"atmCode":null,"ordinal":"1","province":"MELILLA","state":"MELILLA","postalCode":"52006","town":"MELILLA","viaType":"Calle","viaName":"del General Villalba","number":"30","rdo":null,"success":3,"source":"geo-cartociudad-20170727-125627","subcategory":"UNICAJA BANCO"}
````


2. Consulta Geolocalizada


POST geo-cajeros/_search

```json

{
  "query": {
    "geo_distance": { 
      "location":[
        -2.93876416598411,
        35.292602469252195
        ],
        "distance": 100.0,
        "distance_type": "arc",
        "validation_method": "STRICT",
        "ignore_unmapped": false,
        "boost": 1.0
    }
  }
}
```

3. Obtener los datos de Geolocalización a partir de los campos de la via

POST geo-cartociudad-20180205-110605/geolocation/_search
```json

{
  "size" : 500,
  "query" : {
    "match" : {
      "geo_all" : {
        "query" : "Avenida, Rey Juan Carlos I, 1, 52001",
        "operator" : "AND",
        "fuzziness" : "0",
        "prefix_length" : 0,
        "max_expansions" : 50,
        "fuzzy_transpositions" : true,
        "lenient" : false,
        "zero_terms_query" : "NONE",
        "boost" : 1.0
      }
    }
  }
}
```