package com.autentia.pocspringboot;

import com.autentia.pocspringboot.client.impl.ElasticSearchClientImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@RunWith(SpringRunner.class)
@SpringBootTest
public class IndexOperationsIT extends BaseTest {

    @Autowired
    private ElasticSearchClientImpl client;


    @Autowired
    @Qualifier("indexSource")
    private String indexSource;


    @Test
    public void shouldCreateIndex() throws Exception {

        String index = generateIndexWithDateSuffix(PREFFIX_INDEX);

        System.out.println("::: Create Index: " + index);

        final boolean created = client.createIndex(index, indexSource);

        assertThat(created, equalTo(true));

    }


    @Test
    public void shouldReturnRecentIndex() throws Exception {

        String index = generateIndexWithDateSuffix(PREFFIX_INDEX);

        client.createIndex(index, indexSource);

        final String recentIndex = client.getRecentIndexByPrefix(PREFFIX_INDEX + "*");

        System.out.println("::: Recent Index is:" + recentIndex);

        assertThat(recentIndex, equalTo(index));
    }


}
