package com.autentia.pocspringboot;

import com.autentia.pocspringboot.bean.AtmResponse;
import com.autentia.pocspringboot.bean.GeoRequest;
import com.autentia.pocspringboot.bean.GeoResponse;
import com.autentia.pocspringboot.builder.GeoRequestBuilder;
import com.autentia.pocspringboot.repository.GeoDao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SearchOperationsIT extends BaseTest {

    public static final String MAPPING = "geolocation";
    public static final String INDEX_CARTO = "geo-cartociudad-20180205-110605";
    public static final String INDEX_CAJEROS = "geo-cajeros";
    public static final String BUFFER = "200";

    @Autowired
    private GeoDao dao;

    @Test
    public void shouldSearchLocationByViaName() throws Exception {


        GeoRequest location = new GeoRequestBuilder()
                .viaType("Avenida").viaName("Rey Juan Carlos I")
                .number("1").postalcode("52001").build();

        final List<GeoResponse> geoResponses = dao.searchLocation(location, INDEX_CARTO, MAPPING);


        assertThat(geoResponses, hasSize(1));

        printResponses(geoResponses);
    }



    @Test
    public void shouldSearchNearestLocations() throws Exception{

        GeoRequest location = new GeoRequestBuilder().latitude("35.292602469252195").longitude("-2.93876416598411").build();

        final List<AtmResponse> atmResponses = dao.searchNearestLocation(INDEX_CAJEROS, MAPPING, location, BUFFER);

        assertThat(atmResponses, hasSize(5));

        printAtmResponses(atmResponses);
    }



    @Test
    public void shouldReturnByCategoryAndSuccess() throws Exception{

        final List<AtmResponse> atmResponses = dao.searchByCategory(INDEX_CAJEROS, MAPPING, "BBVA", "2");

        assertThat(atmResponses, hasSize(2));

        printAtmResponses(atmResponses);


    }
}
