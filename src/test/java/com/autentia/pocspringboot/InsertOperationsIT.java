package com.autentia.pocspringboot;

import com.autentia.pocspringboot.bean.ErrorESResponse;
import com.autentia.pocspringboot.bean.GeoRequest;
import com.autentia.pocspringboot.repository.GeoDao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

@RunWith(SpringRunner.class)
@SpringBootTest
public class InsertOperationsIT extends BaseTest {

    @Autowired
    private GeoDao dao;

    @Test
    public void shouldInsertLocations() throws Exception{

        final List<GeoRequest> geoRequests = this.buildGeoRequest();

        final String index = dao.getRecentIndexByPrefix(PREFFIX_INDEX + "*");

        final List<ErrorESResponse> errorESResponses = dao.insertLocations(index, "geolocation", geoRequests);

        assertThat(errorESResponses, hasSize(0));
    }
}
