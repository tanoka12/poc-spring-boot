package com.autentia.pocspringboot;

import com.autentia.pocspringboot.bean.AtmResponse;
import com.autentia.pocspringboot.bean.CartoCiudad;
import com.autentia.pocspringboot.bean.GeoRequest;
import com.autentia.pocspringboot.bean.GeoResponse;
import com.autentia.pocspringboot.builder.GeoRequestBuilder;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;

public abstract class BaseTest {

    protected String PREFFIX_INDEX = "geo-cartociudad-";

    @Autowired
    protected CartoCiudad cartoCiudad;

    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd-HHmmss");


    protected String generateIndexWithDateSuffix(String index) throws Exception{

        Thread.sleep(2000);
        String formatDateTime = LocalDateTime.now().format(formatter);
        return index + formatDateTime;

    }


    protected List<GeoRequest> buildGeoRequest() {

        final List<GeoRequest> geoRequestList = cartoCiudad.getFeatures().stream().map(feature ->
                new GeoRequestBuilder(feature).town("Melilla").province("Melilla").state("Melilla").postalcode(feature.getCodPostal()).build()
        ).collect(Collectors.toList());

        return geoRequestList;
    }

    protected void printResponses(List<GeoResponse> geoResponses) {

        System.out.println("\n\n");
        geoResponses.stream().forEach(item -> {
            final String viaName = new StringJoiner(" ")
                    .add(item.getViaType()).add(item.getViaName())
                    .add(item.getNumber()).add(item.getPostalCode()).toString();
            System.out.println("::: " + viaName + ": " + item.getLocation());
        });
        System.out.println("\n\n");
    }


    protected void printAtmResponses(List<AtmResponse> atmResponses) {
        System.out.println("\n\n");
        atmResponses.stream().forEach(item -> {
            final String viaName = new StringJoiner(" ")
                    .add(item.getViaType()).add(item.getViaName())
                    .add(item.getNumber()).add(item.getPostalCode()).toString();
            System.out.println("::: " + viaName + ": " + item.getSubcategory() +  " acierto: " + item.getSuccess() + "\n");
        });
        System.out.println("\n\n");
    }
}
