package com.autentia.pocspringboot.config;


import com.autentia.pocspringboot.bean.CartoCiudad;
import com.autentia.pocspringboot.client.impl.ElasticSearchClientImpl;
import com.autentia.pocspringboot.repository.GeoDao;
import com.autentia.pocspringboot.repository.impl.GeoDaoImpl;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

@Configuration
public class ElasticSearchConfig {

    @Autowired
    private ResourceLoader resourceLoader;

    @Bean
    public RestClient restClient() {
        RestClientBuilder builder = RestClient.builder(new HttpHost("192.168.99.100", 9200));
        return builder.build();
    }

    @Bean
    public ObjectMapper jsonMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(MapperFeature.DEFAULT_VIEW_INCLUSION, true);
        return mapper;
    }

    @Bean
    public ElasticSearchClientImpl elasticClient() {
        return new ElasticSearchClientImpl(restClient(), jsonMapper());
    }

    @Bean
    public CartoCiudad cartoCiudad() throws Exception {

        Resource resource = resourceLoader.getResource("classpath:MELILLA.geojson");


        return jsonMapper().readValue(resource.getFile(), CartoCiudad.class);
    }

    @Bean(name = "indexSource")
    public String indexSource() throws Exception {

        Resource resource = resourceLoader.getResource("classpath:indexSource.json");

        JsonNode jsonNode = jsonMapper().readTree(resource.getFile());

        return jsonNode.toString();
    }


    @Bean
    public GeoDao geoDao() {
        return new GeoDaoImpl(elasticClient(), jsonMapper());
    }
}
