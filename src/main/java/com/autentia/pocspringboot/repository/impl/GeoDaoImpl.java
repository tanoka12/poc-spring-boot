package com.autentia.pocspringboot.repository.impl;

import com.autentia.pocspringboot.bean.*;
import com.autentia.pocspringboot.client.impl.ElasticSearchClientImpl;
import com.autentia.pocspringboot.repository.GeoDao;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.elasticsearch.action.bulk.BulkItemResponse;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchScrollRequest;
import org.elasticsearch.common.unit.DistanceUnit;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.Scroll;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;

public class GeoDaoImpl implements GeoDao {

    protected final ElasticSearchClientImpl client;

    private final ObjectMapper mapper;

    public GeoDaoImpl(ElasticSearchClientImpl client, ObjectMapper mapper) {
        this.client = client;
        this.mapper = mapper;
    }

    @Override
    public List<ErrorESResponse> insertLocations(String index, String mapping, List<? extends ESRequest> locations) throws Exception {
        List<ErrorESResponse> errors = new ArrayList<>();

        if (!locations.isEmpty()) {
            BulkRequest bulkRequest = new BulkRequest();

            locations.forEach(location -> {
                try {
                    bulkRequest.add(new IndexRequest(index, mapping, location.getIndexId())
                            .source(mapper.writeValueAsString(location), XContentType.JSON));
                } catch (JsonProcessingException e) {
                    throw new RuntimeException(e);
                }
            });
            BulkResponse bulkResponse = client.bulk(bulkRequest);
            if (bulkResponse.hasFailures()) {
                errors = buildErrorsResponse(bulkResponse.getItems());
            }

        }
        return errors;

    }


    @Override
    public List<GeoResponse> searchLocation(GeoRequest location, String index, String mapping) throws Exception {


        List<GeoResponse> locations = new ArrayList<>();

        MatchQueryBuilder builder = QueryBuilders.matchQuery("geo_all", buildQueryString(location))
                .operator(Operator.AND)
                .fuzziness(0);

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder().size(500).query(builder);


        SearchRequest searchRequest = new SearchRequest(index).types(mapping).source(searchSourceBuilder);


        System.out.println("::: Query ES  in index : " + index + " " + searchRequest);


        SearchResponse response = client.search(searchRequest);


        System.out.println("::: Response ES :" + response);


        if (response.getHits().getTotalHits() > 0) {

            locations = extractResponse(response, GeoResponse.class);

        }


        return locations;
    }


    @Override
    public List<AtmResponse> searchNearestLocation(String uuidIndex, String mapping, GeoRequest routingLocation, String buffer) throws Exception {

        List<AtmResponse> atmResponses = new ArrayList<>();

        GeoDistanceQueryBuilder qb = QueryBuilders
                .geoDistanceQuery("location")
                .point(Double.parseDouble(routingLocation.getLocation().getLat()),
                        Double.parseDouble(routingLocation.getLocation().getLon()))
                .distance(Double.parseDouble(buffer), DistanceUnit.METERS);


        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder().size(500).query(qb);

        System.out.println("::: Query ES  in index : " + uuidIndex + " " + searchSourceBuilder);

        SearchRequest searchRequest = new SearchRequest(uuidIndex).types(mapping).source(searchSourceBuilder);


        SearchResponse response = client.search(searchRequest);

        System.out.println("::: Response ES :" + response);

        if (response.getHits().getTotalHits() > 0) {
            atmResponses = extractResponse(response, AtmResponse.class);
            System.out.println(atmResponses.size());
        }

        return atmResponses;

    }



    @Override
    public List<AtmResponse> searchByCategory(String index, String mapping, String category, String success) throws Exception{

        List<AtmResponse> atmResponses = new ArrayList<>();

        BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();

        boolQuery.must( QueryBuilders.matchQuery( "subcategory", category ))
                .must(QueryBuilders.matchQuery( "success", success ));

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder().size(500).query(boolQuery);

        System.out.println("::: Query ES  in index : " + index + " " + searchSourceBuilder);

        SearchRequest searchRequest = new SearchRequest(index).types(mapping).source(searchSourceBuilder);


        SearchResponse response = client.search(searchRequest);

        System.out.println("::: Response ES :" + response);

        if (response.getHits().getTotalHits() > 0) {
            atmResponses = extractResponse(response, AtmResponse.class);
            System.out.println(atmResponses.size());
        }

        return atmResponses;
    }

    private Object buildQueryString(GeoRequest location) {

        StringJoiner queryString = new StringJoiner(", ");

        queryString.add(location.getViaType()).add(location.getViaName()).add(location.getNumber()).add(location.getPostalCode());

        return queryString.toString();
    }


    private <T> List<T> extractResponse(SearchResponse response, Class<T> clazz) {
        return Arrays
                .stream(response.getHits().getHits()).map(searchHitFields -> {
                    try {
                        return mapper.readValue(searchHitFields.getSourceAsString(), clazz);
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }).collect(Collectors.toList());
    }

    @Override
    public boolean createIndex(String index) throws Exception {
        return client.createIndex(index);
    }

    @Override
    public boolean createIndex(String index, String indexJsonSource) throws Exception {
        return client.createIndex(index, indexJsonSource);
    }


    @Override
    public String getRecentIndexByPrefix(String preffixIndex) throws Exception {
        return client.getRecentIndexByPrefix(preffixIndex);
    }

    @Override
    public boolean deleteIndex(String index) throws Exception {
        return client.deleteIndex(index);
    }

    @Override
    public GetResponse findObjectById(String index, String mapping, String id) throws Exception {

        GetRequest getRequest = new GetRequest(index, mapping, id);

        return client.get(getRequest);
    }


    protected List<ErrorESResponse> buildErrorsResponse(BulkItemResponse[] items) {
        List<ErrorESResponse> errors = Arrays.stream(items).filter(item -> item.isFailed())
                .map(item -> new ErrorESResponse(item.getId(), item.getFailureMessage()))
                .collect(Collectors.toList());
        return errors;
    }


    @Override
    public <T> List<T> getAllDocuments(String index, Class<T> clazz) throws Exception {

        List<T> result = new ArrayList<>();

        final Scroll scroll = new Scroll(TimeValue.timeValueMinutes(1L));

        final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder().size(1000);

        SearchRequest searchRequest = new SearchRequest(index).scroll(scroll).source(searchSourceBuilder);

        SearchResponse searchResponse = client.search(searchRequest);

        String scrollId = searchResponse.getScrollId();

        SearchHit[] searchHits = searchResponse.getHits().getHits();

        while (searchHits != null && searchHits.length > 0) {

            final List<T> partialResult = extractResponse(searchResponse, clazz);

            result.addAll(partialResult);

            SearchScrollRequest scrollRequest = new SearchScrollRequest(scrollId).scroll(scroll);

            searchResponse = client.searchScroll(scrollRequest);

            scrollId = searchResponse.getScrollId();

            searchHits = searchResponse.getHits().getHits();
        }

        return result;
    }

}
