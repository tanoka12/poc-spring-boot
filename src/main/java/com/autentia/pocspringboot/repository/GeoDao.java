package com.autentia.pocspringboot.repository;

import com.autentia.pocspringboot.bean.*;
import com.autentia.pocspringboot.client.ElasticSearchClient;
import org.elasticsearch.action.get.GetResponse;

import java.util.List;

public interface GeoDao extends ElasticSearchClient {

    List<ErrorESResponse> insertLocations(String index, String mapping, List<? extends ESRequest> locations) throws Exception;

    List<GeoResponse> searchLocation(GeoRequest location, String index, String mapping) throws Exception;

    List<AtmResponse> searchNearestLocation(String uuidIndex, String mapping, GeoRequest routingLocation, String buffer) throws Exception;

    List<AtmResponse> searchByCategory(String index, String mapping, String category, String success) throws Exception;

    GetResponse findObjectById(String index, String mapping, String id) throws Exception;

    <T> List<T> getAllDocuments(String index, Class<T> clazz) throws Exception;
}
