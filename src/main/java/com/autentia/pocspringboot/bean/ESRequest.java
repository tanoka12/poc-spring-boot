package com.autentia.pocspringboot.bean;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.UUID;

public interface ESRequest {
	
	@JsonIgnore
	default public String getIndexId() {
		return UUID.randomUUID().toString();
	}
	
}
