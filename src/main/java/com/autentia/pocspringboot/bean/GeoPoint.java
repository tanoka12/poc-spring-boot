package com.autentia.pocspringboot.bean;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;


@SuppressWarnings("serial")
public class GeoPoint implements Serializable {

    private final String lon;

    private final String lat;

    @JsonCreator
    public GeoPoint(@JsonProperty("lon") String lon, @JsonProperty("lat") String lat) {
        super();
        this.lon = lon;
        this.lat = lat;
    }


    public String getLon() {
        return lon;
    }

    public String getLat() {
        return lat;
    }

    @Override
    public String toString() {
        return "GeoPoint [lon=" + lon + ", lat=" + lat + "]";
    }

}
