package com.autentia.pocspringboot.bean;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class GeoResponse {

    protected static final String OPENADDRESS = "OPENADDRESS";
    protected static final String CARTOCIUDAD = "CARTOCIUDAD";

    private final GeoPoint location;

    private final String number;

    private final String viaName;

    private final String viaType;

    private final String town;

    private final String province;

    private final String postalCode;

    private final String state;

    private String source;

    private int success;


    @JsonCreator
    public GeoResponse(@JsonProperty("location") GeoPoint location, @JsonProperty("number") String number,
                       @JsonProperty("via_name") String viaName, @JsonProperty("via_type") String viaType,
                       @JsonProperty("town") String town,
                       @JsonProperty("province") String province,
                       @JsonProperty("postalCode") String postalCode,
                       @JsonProperty("state") String state) {


        super();
        this.location = location;
        this.number = number;
        this.viaName = viaName;
        this.viaType = viaType;
        this.town = town;
        this.province = province;
        this.postalCode = postalCode;
        this.state = state;
    }

    public GeoPoint getLocation() {
        return location;
    }

    public String getNumber() {
        return number;
    }

    public String getViaName() {
        return viaName;
    }

    public String getViaType() {
        return viaType;
    }

    public String getTown() {
        return town;
    }

    public String getProvince() {
        return province;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public String getState() {
        return state;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }


}
