package com.autentia.pocspringboot.bean;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CartoCiudad {

    @JsonProperty("features")
    private List<GeoJson> features = new ArrayList<GeoJson>();

    /**
     *
     * @return The features
     */
    @JsonProperty("features")
    public List<GeoJson> getFeatures() {
        return features;
    }

    /**
     *
     * @param features
     *        The features
     */
    @JsonProperty("features")
    public void setFeatures(List<GeoJson> features) {
        this.features = features;
    }
}
