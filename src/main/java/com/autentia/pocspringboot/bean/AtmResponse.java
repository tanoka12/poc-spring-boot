package com.autentia.pocspringboot.bean;

public class AtmResponse {


    private GeoPoint location;

    private String id;

    private String red4B;

    private String red6000;

    private String servired;

    private String entityCode;

    private String entity;

    private String branchCode;

    private String branch;

    private String atmCode;

    private String ordinal;

    private String province;

    private String state;

    private String postalCode;

    private String town;

    private String viaType;

    private String viaName;

    private String number;

    private String rdo;

    private int success;

    private String source;

    private String subcategory;

    public AtmResponse() {

    }

    public GeoPoint getLocation() {
        return location;
    }

    public void setLocation(GeoPoint location) {
        this.location = location;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRed4B() {
        return red4B;
    }

    public void setRed4B(String red4B) {
        this.red4B = red4B;
    }

    public String getRed6000() {
        return red6000;
    }

    public void setRed6000(String red6000) {
        this.red6000 = red6000;
    }

    public String getServired() {
        return servired;
    }

    public void setServired(String servired) {
        this.servired = servired;
    }

    public String getEntityCode() {
        return entityCode;
    }

    public void setEntityCode(String entityCode) {
        this.entityCode = entityCode;
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getAtmCode() {
        return atmCode;
    }

    public void setAtmCode(String atmCode) {
        this.atmCode = atmCode;
    }

    public String getOrdinal() {
        return ordinal;
    }

    public void setOrdinal(String ordinal) {
        this.ordinal = ordinal;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getViaType() {
        return viaType;
    }

    public void setViaType(String viaType) {
        this.viaType = viaType;
    }

    public String getViaName() {
        return viaName;
    }

    public void setViaName(String viaName) {
        this.viaName = viaName;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getRdo() {
        return rdo;
    }

    public void setRdo(String rdo) {
        this.rdo = rdo;
    }

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(String subcategory) {
        this.subcategory = subcategory;
    }
}
