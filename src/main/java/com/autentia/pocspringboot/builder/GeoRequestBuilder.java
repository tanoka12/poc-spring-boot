package com.autentia.pocspringboot.builder;


import com.autentia.pocspringboot.bean.GeoJson;
import com.autentia.pocspringboot.bean.GeoPoint;
import com.autentia.pocspringboot.bean.GeoRequest;
import org.apache.commons.lang3.StringUtils;

public class GeoRequestBuilder {

    private String lon;

    private String lat;

    private String number;

    private String town;

    private String province;

    private String postalCode;

    private String viaName;

    private String viaType;

    private String state;

    public GeoRequestBuilder() {

    }


    public GeoRequestBuilder(GeoJson geoJson) {

        if (geoJson.getGeo() != null) {
            this.lon = geoJson.getGeo().getLongitude();
            this.lat = geoJson.getGeo().getLatitude();
        }

        this.number = geoJson.getNumber();
        this.postalCode = geoJson.getCodPostal();
        this.viaName = geoJson.getViaName();
        this.viaType = geoJson.getViaType();

    }

    private void fillVia(String via) {

        if (StringUtils.isNotBlank(via) && via.length() >= 3) {
            this.viaType = via.substring(0, 2);
            this.viaName = via.substring(3);
        } else {
            this.viaName = via;
            this.viaType = "";
        }

    }


    public GeoRequest build() {

        GeoPoint location = new GeoPoint(lon, lat);

        GeoRequest geoRequest = new GeoRequest(location, number, town, province, postalCode, viaName, viaType, state);
        return geoRequest;
    }

    public GeoRequestBuilder latitude(String lat) {
        this.lat = lat;
        return this;
    }

    public GeoRequestBuilder longitude(String lon) {
        this.lon = lon;
        return this;
    }

    public GeoRequestBuilder province(String province) {
        this.province = province;
        return this;
    }

    public GeoRequestBuilder number(String number) {
        this.number = number;
        return this;
    }

    public GeoRequestBuilder postalcode(String postalcode) {
        this.postalCode = postalcode;
        return this;
    }

    public GeoRequestBuilder viaName(String viaName) {
        this.viaName = viaName;
        return this;
    }

    public GeoRequestBuilder viaType(String viaType) {
        this.viaType = viaType;
        return this;
    }

    public GeoRequestBuilder state(String state) {
        this.state = state;
        return this;
    }

    public GeoRequestBuilder town(String town) {
        this.town = town;
        return this;
    }

}
