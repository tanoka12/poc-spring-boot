package com.autentia.pocspringboot.client;

public interface ElasticSearchClient {


    String getRecentIndexByPrefix(String preffixIndex) throws Exception;

    boolean createIndex(String index) throws Exception;

    boolean createIndex(String index, String indexSettings) throws Exception;

    boolean deleteIndex(String index) throws Exception;
}
